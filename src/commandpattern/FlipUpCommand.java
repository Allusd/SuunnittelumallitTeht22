/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commandpattern;

/**
 *
 * @author aleks
 */
public class FlipUpCommand implements Command {
    
    private Valkokangas VK;
    public FlipUpCommand(Valkokangas vk){
        this.VK = vk;
    }
    
    @Override
    public void execute() {
        VK.turnOn();
        
    }
    
}
