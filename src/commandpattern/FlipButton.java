/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commandpattern;

/**
 *
 * @author aleks
 */
public class FlipButton {
    IFlipCommand Icmd;

    public FlipButton(IFlipCommand Icmd){
        this.Icmd = Icmd;
    }

    public void flipUp(){
        Icmd.execute();
    }
    public void flipDown(){
        Icmd.unexecute();
    }
    
}
