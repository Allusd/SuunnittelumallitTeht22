/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commandpattern;

/**
 *
 * @author aleks
 */
public class TestValkokangas {
    public static void main(String[] args){
        Valkokangas VK = new Valkokangas();
        Command up = new FlipUpCommand(VK);
        Command down = new FlipDownCommand(VK);
        IFlipCommand both = new FlipUpDownCommand(VK);
        WallButton button1 = new WallButton(up);
        WallButton button2 = new WallButton(down);
        FlipButton flip = new FlipButton(both);
        
        button1.push();
        button2.push();
        flip.flipUp();
        flip.flipDown();
        
    }
}
