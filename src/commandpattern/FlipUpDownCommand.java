/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commandpattern;

/**
 *
 * @author aleks
 */
public class FlipUpDownCommand  implements IFlipCommand {
    private Valkokangas VK;
    
    public FlipUpDownCommand(Valkokangas vk){
        this.VK = vk;
    }

    @Override
    public void execute() {
        VK.turnOn();
    }

    @Override
    public void unexecute() {
        VK.turnOff();
    }
    
}

