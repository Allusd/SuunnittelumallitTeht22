/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commandpattern;

/**
 *
 * @author aleks
 */
public class WallButton {
    Command cmd;
    IFlipCommand Icmd;
    public WallButton(Command cmd){
        this.cmd = cmd;
    }
    public WallButton(IFlipCommand Icmd){
        this.Icmd = Icmd;
    }
    public void push(){
        cmd.execute();
    }
    public void flipUp(){
        Icmd.execute();
    }
    public void flipDown(){
        Icmd.unexecute();
    }
    
}
